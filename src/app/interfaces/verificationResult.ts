export interface VerificationResult {
  confidence: number;
  isIdentical: boolean;
}
