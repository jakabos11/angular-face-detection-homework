import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetectionComponent } from './detection/detection.component';
import { RecognitionComponent } from './recognition/recognition.component';

const routes: Routes = [
  { path: 'detection', component: DetectionComponent },
  { path: 'recognition', component: RecognitionComponent },
  { path: '**', redirectTo: 'detection' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
