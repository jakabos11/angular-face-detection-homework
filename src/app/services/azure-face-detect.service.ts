import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Face } from '../interfaces/face';
import { environment } from 'src/environments/environment';
import { combineLatest, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { VerificationResult } from '../interfaces/verificationResult';

@Injectable({
  providedIn: 'root',
})
export class AzureFaceDetectService {
  constructor(private http: HttpClient, private domsanitizer: DomSanitizer) {}
  faceResponseData$: Observable<Face[]>;
  faceVerificationData$: Observable<{
    verificationResult: VerificationResult;
    firstFace: Face;
    secondFace: Face;
  }>;
  //in a real-life scenario I would not commit this, but this is not sensitive information because of the simplicity of this homework
  key = '88aa119a125245de910879acbc186a72';
  selectedFile: SafeUrl;
  verifiedFiles: SafeUrl[] = [];
  detectionImage = new Image();
  verificationImages: any[] = [new Image(), new Image()];
  loading: boolean = false;

  /**
   * Sends detect request to Azure Face API
   * @param file - uploaded file to detect face on
   * @returns result face data
   */
  detectFaceWithAzure(file: File): Observable<Face[]> {
    return this.http
      .post<Face[]>(`${environment.apiUrl}/detect`, file, {
        params: {
          returnFaceId: 'true',
          returnFaceAttributes:
            'blur,exposure,noise,age,gender,facialhair,glasses,hair,makeup,accessories,occlusion,headpose,emotion,smile',
          recognitionModel: 'recognition_04',
        },
        headers: {
          'Ocp-Apim-Subscription-Key': this.key,
          'Content-Type': 'application/octet-stream',
        },
      })
      .pipe(
        tap(() => {
          this.loading = false;
        })
      );
  }

  /**
   *  Calls the face detect function,
   *  sets the appropriate variables with the result data
   * @param selectedFile - input file
   */
  processImageForFaceDetection(selectedFile: File) {
    this.loading = true;
    const reader = new FileReader();

    reader.onload = (e) => {
      this.selectedFile = this.domsanitizer.bypassSecurityTrustUrl(
        e.target?.result as string
      );
      this.detectionImage.src = e.target?.result as string;
    };
    reader.readAsDataURL(new Blob([selectedFile]));
    this.faceResponseData$ = this.detectFaceWithAzure(selectedFile);
  }


  /**
   * Calls the face detect function,
   * gets the ID of the first 2 faces and sends verification requests to the API
   * sets the appropriate variables to result data
   * @param selectedFiles - input file array
   */
  sendImagesToVerifyToAzureFaceDetectionApi(selectedFiles: File[]) {
    this.loading = true;

    Object.values(selectedFiles).forEach((file, idx) => {
      const reader = new FileReader();
      reader.onload = (e) => {
        this.verifiedFiles[idx] = this.domsanitizer.bypassSecurityTrustUrl(
          e.target?.result as string
        );

        this.verificationImages[idx].src = e.target?.result as string;
      };
      reader.readAsDataURL(new Blob([file]));
    });
    const firstFaceID = this.detectFaceWithAzure(selectedFiles[0]);
    const secondFaceID = this.detectFaceWithAzure(selectedFiles[1]);
    this.faceVerificationData$ = combineLatest([
      firstFaceID,
      secondFaceID,
    ]).pipe(
      switchMap(([first, second]) => {
        return this.http
          .post<VerificationResult>(
            `${environment.apiUrl}/verify`,
            { faceId1: first[0].faceId, faceId2: second[0].faceId },
            {
              params: {
                returnFaceId: 'true',
              },
              headers: {
                'Ocp-Apim-Subscription-Key': this.key,
                'Content-Type': 'application/json',
              },
            }
          )
          .pipe(
            map((confidence) => {
              return {
                verificationResult: confidence,
                firstFace: first[0],
                secondFace: second[0],
              };
            })
          );
      }),
      tap(() => {
        this.loading = false;
      })
    );
  }
}
