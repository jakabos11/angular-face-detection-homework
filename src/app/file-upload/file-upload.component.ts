import { Component, Input } from '@angular/core';
import { AzureFaceDetectService } from '../services/azure-face-detect.service';

@Component({
  selector: 'file-upload',
  templateUrl: 'file-upload.component.html',
  styleUrls: ['file-upload.component.scss'],
})
export class FileUploadComponent {
  constructor(private azureFaceDetectService: AzureFaceDetectService) {}
  @Input()
   onFileSelectedAction: (event: any)=>void
/**
 * Calls the input function with given parameter
 * @param event
 */
  onFileSelected(event: any) {
    this.onFileSelectedAction(event);
  }
}
