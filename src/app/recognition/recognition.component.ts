import { Component, OnInit } from '@angular/core';
import { AzureFaceDetectService } from '../services/azure-face-detect.service';

@Component({
  selector: 'recognition',
  templateUrl: './recognition.component.html',
  styleUrls: ['./recognition.component.scss'],
})
export class RecognitionComponent {
  /**
   * Function passed as parameter to the FileUpload component
   * Selects first 2 of the uploaded files and sends them to api
   * @param event
   * @returns - early return if error occured
   */
  onFileUploaded(event: any) {
    if (event.target.files.length < 2) {
      alert('Please upload 2 files!');
      return;
    }
    this.azureFaceDetectService.sendImagesToVerifyToAzureFaceDetectionApi(
      event.target.files
    );
  }

  constructor(public azureFaceDetectService: AzureFaceDetectService) {}
}
