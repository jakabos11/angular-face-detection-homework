import { Component, Input } from '@angular/core';
import { Face } from '../interfaces/face';

@Component({
  selector: 'detection-result',
  templateUrl: './detection-result.component.html',
  styleUrls: ['./detection-result.component.scss'],
})
export class DetectionResultComponent {
  panelOpenState = false;
  @Input() face: Face;
}
