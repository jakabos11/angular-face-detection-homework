import { Component } from '@angular/core';
import { AzureFaceDetectService } from '../services/azure-face-detect.service';

@Component({
  selector: 'detection',
  templateUrl: './detection.component.html',
  styleUrls: ['./detection.component.scss'],
})
export class DetectionComponent {
  constructor(public azureFaceDetectService: AzureFaceDetectService) {}
  /**
   * Function passed as input to the FileUpload component,
   *  selects first file from the uploaded input and
   *  sends api request to get data
   * @param event - contains uploaded files as parameter
   */
  onFileUploaded(event: any) {
    const file: File = event.target.files[0];
    this.azureFaceDetectService.processImageForFaceDetection(file);
  }
}
