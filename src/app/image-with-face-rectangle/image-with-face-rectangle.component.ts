import { Component, Input, OnInit } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { Face } from '../interfaces/face';

@Component({
  selector: 'app-image-with-face-rectangle',
  templateUrl: './image-with-face-rectangle.component.html',
  styleUrls: ['./image-with-face-rectangle.component.scss'],
})
export class ImageWithFaceRectangleComponent {
  constructor() {}
  @Input()
  image: SafeUrl;
  @Input()
  faceData: Face[] | null;
  @Input()
  detectionImage = new Image();
}
